class Moradia:
  def __init__(self, area, endereco, numero, preco_imovel, *tipo):
    self.area = area 
    self.endereco = endereco
    self.numero = numero 
    self.preco_imovel = preco_imovel 
    self.tipo = 'Não especificado'
  def gerar_relatorio_preco(self):
    preco = self.preco_imovel/self.area
    return int(preco)


moradia_1 = Moradia(100, 'Rua das Limeiras', 672, 250000)
print(moradia_1.__dict__)
print(moradia_1.gerar_relatorio_preco())


class Apartamento(Moradia):
  tipo = 'Apartamento'
  def __init__(self,area, endereco, numero, preco_imovel, preco_condominio, num_apt, num_elevadores):
    super().__init__(area, endereco, numero, preco_imovel)  
    self.preco_condominio = preco_condominio
    self.num_apt = num_apt
    self.num_elevadores = num_elevadores
    self.andar = self.encontre_andar(num_apt)

  def encontre_andar(self, num_apt):
    num_apt = str(num_apt)
    if len(num_apt) == 2:
      num = list(num_apt)
      self.andar = num[0]
      return self.andar
    else:
      num = list(num_apt)
      apt = [num[0],num[1]]
      apt1 = ''.join(apt)
      apt2 = int(apt1)
      self.andar = apt2 
      return self.andar 

  def gerar_relatorio(self):
    return f'{self.endereco} - apt {self.num_apt} - andar: {self.andar} - elevadores: {self.num_elevadores} - preco por m²: R$ {self.gerar_relatorio_preco()}'    


apt_1 = Apartamento(100, 'Rua das Limeiras', 672, 500000, 700, 110, 2)

print(apt_1.__dict__)

print(apt_1.gerar_relatorio())





